/**
 * Created by pohchye on 13/7/2016.
 */
// Create the Express app
var express=require("express");
var app=express();

// Path module
var path=require("path");

// Require this for POST method
var bodyParser=require("body-parser");
app.use(bodyParser.urlencoded({'extended': 'true'}));
app.use(bodyParser.json());

// Load mysql module
var mysql=require("mysql");
// Create mysql connection pool
var pool = mysql.createPool({
    host: "localhost",
    port: 3306,
    user: "pohchye",
    password: "pohchye",
    database: "employees",
    connectionLimit: 5
});

// Middleware

// Specific employee
app.get("/api/employee/:emp_no", function (req, res) {
    // console.info(req);
    console.info("Employee Number: " + req.params.emp_no);
    // Perform Queries
    pool.getConnection(function(err, connection) {
        if (err) {
            console.info("error connection");
            res.status(500).send(JSON.stringify(err));
            // return;
        } else {
            console.info("Good connection");
            // connection.query("select * from employees where emp_no=" + req.params.emp_no,
            // For more than 1 parameter
            // connection.query("select * from employees where emp_no=?", [req.params.emp_no, ...],
            connection.query("select * from employees where emp_no=?", [req.params.emp_no],
                function (err,results) {
                    connection.release();
                    if (err) {
                        console.info("Query Error");
                        res.status(500).send("Query Error: " + JSON.stringify(err));
                        // return;
                    } else {
                        for (var i in results) {
                            if (results.hasOwnProperty(i)) {
                                console.info(JSON.stringify(results[i]));
                                // console.info(results[i]);
                            }
                        }
                        // res.status(200).end("Accepted emp_no");
                        if (results.length) {
                            res.json(results[0]);
                        } else {
                            res.status(404).end("There are no records");
                        }

                    }
                });
        }
    });

});

// All employees
const GET_ALL_EMPLOYEES = "select emp_no, first_name, last_name, gender, hire_date from employees limit 100";
app.get("/api/employees", function (req, res) {
    console.info(req);
    // Perform Queries
    pool.getConnection(function(err, connection) {
        if (err) {
            console.info("error connection");
            res.status(500).send(JSON.stringify(err));
            // return;
        } else {
            console.info("Good connection");
            // connection.query("select * from employees where emp_no=" + req.params.emp_no,
            // For more than 1 parameter
            // connection.query("select * from employees where emp_no=?", [req.params.emp_no, ...],
            connection.query(GET_ALL_EMPLOYEES,[],
                function (err,results) {
                    connection.release();
                    if (err) {
                        console.info("Query Error");
                        res.status(500).send("Query Error: " + JSON.stringify(err));
                        // return;
                    } else {
                        for (var i in results) {
                            if (results.hasOwnProperty(i)) {
                                console.info(JSON.stringify(results[i]));
                                // console.info(results[i]);
                            }
                        }
                        res.json(results);
                    }
                });
        }
    });

});

// First Name filter
const GET_EMPLOYEES_FIRSTNAME = "select emp_no, first_name, last_name, gender, hire_date from employees " +
    "where first_name like ?";
app.get("/api/employees/firstname/:pattern", function (req, res) {
    console.info("First Name Pattern: " + req.params.pattern);
    // Perform Queries
    pool.getConnection(function(err, connection) {
        if (err) {
            console.info("error connection");
            res.status(500).send(JSON.stringify(err));
            // return;
        } else {
            console.info("Good connection");
            var pattern = "%" + req.params.pattern + "%";
            connection.query(GET_EMPLOYEES_FIRSTNAME, [pattern],
                function (err,results) {
                    connection.release();
                    if (err) {
                        console.info("Query Error");
                        res.status(500).send("Query Error: " + JSON.stringify(err));
                        // return;
                    } else {
                        for (var i in results) {
                            if (results.hasOwnProperty(i)) {
                                console.info(JSON.stringify(results[i]));
                                // console.info(results[i]);
                            }
                        }
                        res.json(results);
                    }
                });
        }
    });

});

// Age filter
const GET_EMPLOYEES_AGE = "select first_name, last_name, birth_date, year(curdate())- year(birth_date) as age from employees " +
    "where year(curdate())- year(birth_date)";
app.get("/api/employees/age/:age_criteria/:age", function (req, res) {
    console.info("Relation: " + req.params.age_criteria);
    console.info("Age : " + req.params.age);
    // Perform Queries
    pool.getConnection(function(err, connection) {
        if (err) {
            console.info("error connection");
            res.status(500).send(JSON.stringify(err));
            // return;
        } else {
            console.info("Good connection");
            // var pattern = "%" + req.params.pattern + "%";
            var sqlAgeCriteria = "";
            if (req.params.age_criteria == "gt") {
                sqlAgeCriteria = " > " + mysql.escape(req.params.age);
            } else if (req.params.age_criteria == "lt") {
                sqlAgeCriteria = " < " + mysql.escape(req.params.age);
            } else if (req.params.age_criteria == "eq") {
                sqlAgeCriteria = " = " + mysql.escape(req.params.age);
            }
            sqlAgeCriteria = sqlAgeCriteria + "limit 0, 10";
            connection.query(GET_EMPLOYEES_AGE + sqlAgeCriteria, [],
                function (err,results) {
                    connection.release();
                    if (err) {
                        console.info("Query Error");
                        res.status(500).send("Query Error: " + JSON.stringify(err));
                        // return;
                    } else {
                        for (var i in results) {
                            if (results.hasOwnProperty(i)) {
                                console.info(JSON.stringify(results[i]));
                                // console.info(results[i]);
                            }
                        }
                        res.json(results);
                    }
            });
        }
    });

});

// Filter using 2 params
const GET_EMPLOYEES_PARAM2 = "select * from employees " +
    "where gender = ? AND Year(birth_date) < ? limit 10";
app.get("/api/employees/gender_birth/:gender/:birth_year", function (req, res) {
    console.info("Gender: " + req.params.gender);
    console.info("Birth Year : " + req.params.birth_year);
    console.info("SQL: " + GET_EMPLOYEES_PARAM2);
    // Perform Queries
    pool.getConnection(function(err, connection) {
        if (err) {
            console.info("error connection");
            res.status(500).send(JSON.stringify(err));
            // return;
        } else {
            console.info("Good connection");
            connection.query(GET_EMPLOYEES_PARAM2, [req.params.gender, req.params.birth_year],
                function (err,results) {
                    connection.release();
                    if (err) {
                        console.info("Query Error");
                        res.status(500).send("Query Error: " + JSON.stringify(err));
                        // return;
                    } else {
                        for (var i in results) {
                            if (results.hasOwnProperty(i)) {
                                console.info(JSON.stringify(results[i]));
                                // console.info(results[i]);
                            }
                        }
                        res.json(results);
                    }
                });
        }
    });

});

// Filter using 2 tables
const GET_DEPT_EMP = "select e.emp_no, e.first_name, e.last_name, d.dept_name " +
    "from employees e, dept_emp p, departments d " +
    "where p.emp_no = e.emp_no AND p.dept_no = d.dept_no AND d.dept_no = ? limit 10";
app.get("/api/employees/searchEmployees/by/dept/:dept_no", function (req, res) {
    console.info("SQL: " + GET_DEPT_EMP);
    console.info("Dept Name: " + req.params.dept_no);
    // Perform Queries
    pool.getConnection(function(err, connection) {
        if (err) {
            console.info("error connection");
            res.status(500).send(JSON.stringify(err));
            // return;
        } else {
            console.info("Good connection");
            connection.query(GET_DEPT_EMP, [req.params.dept_no],
                function (err,results) {
                    connection.release();
                    if (err) {
                        console.info("Query Error");
                        res.status(500).send("Query Error: " + JSON.stringify(err));
                        // return;
                    } else {
                        for (var i in results) {
                            if (results.hasOwnProperty(i)) {
                                console.info(JSON.stringify(results[i]));
                                // console.info(results[i]);
                            }
                        }
                        res.json(results);
                    }
                });
        }
    });

});

// Serve public files
app.use(express.static(__dirname + "/public"));
// - For bower_components that is on the root directory instead of public
// app.use("/bower_components",express.static(path.join(__dirname, "bower_components")));
// app.use(express.static(path.join(__dirname,"public")));
// Handle Errors
app.use(function (req, res, http) {
    res.redirect("error.html");
});

// Set port
app.set("port",
    process.argv[2] || process.env.APP_PORT || 3011
);

// Start the Express server
app.listen(app.get("port"), function () {
    console.info("Express server started on port %s", app.get("port"));
});