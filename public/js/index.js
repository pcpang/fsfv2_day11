/**
 * Created by pohchye on 13/7/2016.
 */
var EmployeeApp = angular.module("EmployeeApp", []);

(function () {
    var EmployeeCtrl;
    EmployeeCtrl = function ($http, $window) {
        var ctrl = this;
        ctrl.emp_no = 0;
        ctrl.result = null;
        ctrl.get_employees = function () {
            $http.get("/api/employees")
                .then(function (result) {
                    ctrl.result = result.data;
                })
                .catch(function (error) {
                    ctrl.error = error.data;
                })
        };
    };
    
    EmployeeApp.controller("EmployeeCtrl", [$http, $window, EmployeeCtrl]);
})();